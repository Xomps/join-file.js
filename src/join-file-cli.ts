#!/usr/bin/env node
import join from './join-file'

async function start () {
  const file = process.argv[2]
  let result: string | void = ""
  
  if (!file) {
    console.error(`
  [Error] File not supplied.
    e.g. join-file my-big-file.7z.001
    `)
    return
  }

  console.log(`
  [Info] Joining "${file}"
  `)
  result = await join(file)
    .catch(e => console.error(`  [Error] ${e}
    `))
 
  if (result && result.length) {
    console.log(`  [Success] OK.
    `)
  }  
}

start()