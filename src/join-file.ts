import { stat, appendFile, unlink, readFile, Stats } from 'fs'
import { promisify } from 'util'

const pstat = promisify(stat)
const pappendFile = promisify(appendFile)
const preadFile = promisify(readFile)
const punlink = promisify(unlink)

export default async function join (filePath: string): Promise<string> {
  if (!(await _exists(filePath))) {
    throw new Error('Input file does not exist')    
  }
  const parts = await _getParts(filePath)

  if (parts.length < 1) {
    throw new Error('Partial files not found')        
  }

  const outFile = filePath.replace(/\.\d{3}$/, '')

  if (await _exists(outFile)) {
    throw new Error('Output file already exists')
  }

  return await _joinFiles(parts, outFile)
}

async function _getParts (filePath: string): Promise<string[]> {
  const files: string[] = []
  const filename = filePath.replace(/\.\d{3}$/, '')
  let counter = 1
  
  await (async function next () {
    let name = `${filename}.${`${counter++}`.padStart(3, '0')}`
    try {
      if (await _exists(name)) {
        files.push(name)
        await next()
      }
    } catch (e) {
      // console.log('error:', name)      
    }
  })()

  return files
}

async function _exists (filePath: string): Promise<boolean> {
  try {
    return (await pstat(filePath)).isFile()
  } catch {
    return false
  }
}

async function _joinFiles (filesPath: string[], outFilePath: string): Promise<string> {
  for (let filePath of filesPath) {
    await pappendFile(outFilePath, await preadFile(filePath))
  }
  return outFilePath  
}
