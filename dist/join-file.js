"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const util_1 = require("util");
const pstat = util_1.promisify(fs_1.stat);
const pappendFile = util_1.promisify(fs_1.appendFile);
const preadFile = util_1.promisify(fs_1.readFile);
const punlink = util_1.promisify(fs_1.unlink);
function join(filePath) {
    return __awaiter(this, void 0, void 0, function* () {
        if (!(yield _exists(filePath))) {
            throw new Error('Input file does not exist');
        }
        const parts = yield _getParts(filePath);
        if (parts.length < 1) {
            throw new Error('Partial files not found');
        }
        const outFile = filePath.replace(/\.\d{3}$/, '');
        if (yield _exists(outFile)) {
            throw new Error('Output file already exists');
        }
        return yield _joinFiles(parts, outFile);
    });
}
exports.default = join;
function _getParts(filePath) {
    return __awaiter(this, void 0, void 0, function* () {
        const files = [];
        const filename = filePath.replace(/\.\d{3}$/, '');
        let counter = 1;
        yield (function next() {
            return __awaiter(this, void 0, void 0, function* () {
                let name = `${filename}.${`${counter++}`.padStart(3, '0')}`;
                try {
                    if (yield _exists(name)) {
                        files.push(name);
                        yield next();
                    }
                }
                catch (e) {
                    // console.log('error:', name)      
                }
            });
        })();
        return files;
    });
}
function _exists(filePath) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            return (yield pstat(filePath)).isFile();
        }
        catch (_a) {
            return false;
        }
    });
}
function _joinFiles(filesPath, outFilePath) {
    return __awaiter(this, void 0, void 0, function* () {
        for (let filePath of filesPath) {
            yield pappendFile(outFilePath, yield preadFile(filePath));
        }
        return outFilePath;
    });
}
