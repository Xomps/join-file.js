#!/usr/bin/env node
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const join_file_1 = __importDefault(require("./join-file"));
function start() {
    return __awaiter(this, void 0, void 0, function* () {
        const file = process.argv[2];
        let result = "";
        if (!file) {
            console.error(`
  [Error] File not supplied.
    e.g. join-file my-big-file.7z.001
    `);
            return;
        }
        console.log(`
  [Info] Joining "${file}"
  `);
        result = yield join_file_1.default(file)
            .catch(e => console.error(`  [Error] ${e}
    `));
        if (result && result.length) {
            console.log(`  [Success] OK.
    `);
        }
    });
}
start();
