***
# This package is deprecated. Please use __[axnglu](https://npmjs.org/axnglu)__ instead.
***

# join-file.js

> Join files in the form `original.ext.001 original.ext.002 original.ext.003 ...` into `original.ext`.

```
Input:
  my-file.7z.001 - 100MB
  my-file.7z.002 - 100MB
  my-file.7z.003 - 100MB
  my-file.7z.004 -  22MB

Output:
  my-file.7z     - 322MB

```

## Node.JS

#### Install

```
npm install join-file.js
```

#### API
```typescript
import join from 'join-file.js'

join (
  filePath: string,
  deleteOriginalFile = false
) => Promise<string>

```
> `filePath` doesn't need to be the first part.
> Returns a `Promise` that fulfills into the path of the resulting file.

#### Examples

```javascript
// Import module
import joinFile from 'join-file.js'


// Join the partial files
const result = await joinFile('C:/some-dir/my-large-file.7z.001')
// or
const result = await joinFile('C:/some-dir/my-large-file.7z.013')


// Join the files and delete the original partial files
const files = await joinFile(myFile, true)

```


## CLI

#### Install

```
npm install -g join-file.js
```

#### Usage

```
$ join-file <file-path>
```

